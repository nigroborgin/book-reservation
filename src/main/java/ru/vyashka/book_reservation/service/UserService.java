package ru.vyashka.book_reservation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.vyashka.book_reservation.dao.UserDAO;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserDAO userDAO;

    public List getAllUsers() {
        return userDAO.getAllUsers();
    }
}
