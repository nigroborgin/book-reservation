package ru.vyashka.book_reservation.entity;

import java.util.ArrayList;
import java.util.List;

public class Book {
    private int id;
    private String bookName;
    private boolean statusAvailable;
    private List<Author> authorsThisBook;

    public Book() {}

    public Book(String book_name, boolean statusIsAvailable) {
        this.bookName = book_name;
        this.statusAvailable = statusIsAvailable;
    }

    public void addAuthorToBook(Author author) {
        if (authorsThisBook == null) {
            authorsThisBook = new ArrayList<>();
        }
        authorsThisBook.add(author);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public boolean isStatusAvailable() {
        return statusAvailable;
    }

    public void setStatusAvailable(boolean statusAvailable) {
        this.statusAvailable = statusAvailable;
    }

    //TODO: проверить правильность equals, hashCode, toString

}
