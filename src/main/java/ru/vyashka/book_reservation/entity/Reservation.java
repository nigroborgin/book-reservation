package ru.vyashka.book_reservation.entity;

import java.time.LocalDateTime;
import java.util.Objects;

public class Reservation {
    private User user;
    private Book book;
    private LocalDateTime takeDate;
    private LocalDateTime returnDate;

    public Reservation() {}

    public Reservation(User user, Book book, LocalDateTime takeDate, LocalDateTime returnDate) {
        this.user = user;
        this.book = book;
        this.takeDate = takeDate;
        this.returnDate = returnDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public LocalDateTime getTakeDate() {
        return takeDate;
    }

    public void setTakeDate(LocalDateTime takeDate) {
        this.takeDate = takeDate;
    }

    public LocalDateTime getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDateTime returnDate) {
        this.returnDate = returnDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Reservation)) return false;
        Reservation that = (Reservation) o;
        return Objects.equals(user, that.user) && Objects.equals(book, that.book) && Objects.equals(takeDate, that.takeDate) && Objects.equals(returnDate, that.returnDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, book, takeDate, returnDate);
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "user=" + user +
                ", book=" + book +
                ", takeDate=" + takeDate +
                ", returnDate=" + returnDate +
                '}';
    }
}
