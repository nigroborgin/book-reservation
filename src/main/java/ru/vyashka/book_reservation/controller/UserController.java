package ru.vyashka.book_reservation.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.vyashka.book_reservation.entity.User;
import ru.vyashka.book_reservation.service.UserService;

import java.util.List;

@Controller
public class UserController {

    private UserService userService;

    @RequestMapping("/users")
    public List<User> showUsersView() {
        return userService.getAllUsers();
    }
}
