create schema book_reservation;


create table IF NOT EXISTS book_reservation.users
(
    id int
        constraint USERS_PK
            primary key,
    name varchar(45),
    password varchar(45)
);


create table IF NOT EXISTS book_reservation.authors
(
    id int
        constraint AUTHORS_PK
            primary key,
    name varchar(45),
    surname varchar(45) not null
);

create table IF NOT EXISTS book_reservation.books
(
    id int
        constraint BOOKS_PK
            primary key,
    book_name varchar(100) not null,
    status_is_available boolean
);


create table IF NOT EXISTS book_reservation.users_has_books
(
    user_id int not null
        constraint USERS_HAS_BOOKS_USERS_ID_FK
            references USERS,
    book_id int not null
        constraint USERS_HAS_BOOKS_BOOKS_ID_FK
            references BOOKS,
    take_date timestamp not null,
    return_date timestamp not null,
    constraint USERS_HAS_BOOKS_PK
        primary key (user_id, book_id)
);


create table IF NOT EXISTS book_reservation.books_has_authors
(
    book_id int
        constraint BOOKS_HAS_AUTHORS_BOOKS_ID_FK
            references BOOKS,
    author_id int
        constraint BOOKS_HAS_AUTHORS_AUTHORS_ID_FK
            references AUTHORS,
    constraint BOOKS_HAS_AUTHORS_PK
        primary key (book_id, author_id)
);
